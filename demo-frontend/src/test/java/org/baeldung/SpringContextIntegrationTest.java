package org.baeldung;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.baeldung.spring.cloud.kubernetes.frontend.KubernetesFrontendApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = KubernetesFrontendApplication.class)
public class SpringContextIntegrationTest {

	@Value("${server.url}")
	private String url;

	@Value("${smoke.response}")
	private String response;

	@Test
	public void contextLoads() {
	}
	
	@Test
	public void basicSmokeTest() {
	    

		
		String testUrl = url;
		System.out.println("Starting front-end test...");
		System.out.println("Expected response: " + response);
		System.out.println("Test URL: " + testUrl);
		
	    given().
	    when().
	        get(testUrl).
	    then().
	    assertThat().
	        body(containsString(response));
	}

}
